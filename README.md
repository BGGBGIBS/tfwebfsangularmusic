## Créer le projet 
- ng new nom-du-projet (choisir avec routing et scss)

## Lancer le serveur
- ng serve
- ng serve --open (pour que le projet se lance direct dans notre navigateur favoris)
- ng s --o (la version flemmard)

## Pour créer un composant
- ng generate component eventuel-chemin/nom-du-component
- ng g c eventuel-chemin/nom-du-component (la version flemme)